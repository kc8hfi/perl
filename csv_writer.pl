#!/usr/bin/perl

use strict;
use warnings;
use Text::CSV_XS;
use Data::Dumper;

my $csv = Text::CSV_XS->new( 
    {
        always_quote => 1, binary=>1
    }
);


my @colheaders = ("subject","course","title");
my @array = ("cs","365","perl");
my @array2 = ("cs","324","database");

open my $fh ,">courses.csv" or die "cannot create file!  $! \n";

$csv->combine(@colheaders);
print $fh $csv->string(),"\n";

$csv->combine(@array);
print $fh $csv->string(), "\n";

$csv->combine(@array2);
print $fh $csv->string(), "\n";

close $fh;
