#!/usr/bin/perl

use strict;
use warnings;

#number between 0 and 1, not including 1
print rand(),"\n";

#number between 0 and 11,  but not including 11
print rand(11),"\n";

print "same set of random numbers each run!\n";
#same set of random numbers on every run
print srand(24),"\n";
print rand(),"\n";
print rand(),"\n";
print rand(5),"\n";



#just get the whole number part of the number
print int rand(12),"\n";
