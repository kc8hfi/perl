#!/usr/bin/perl

use strict;
use warnings;


#m/#EXTINF/)


my $file1 = "/home/student/programming/perl/arrays.pl";
my $file2 = "/home/student/programming/perl/regex_examples.pl";

if ($file1 =~ m/\/home\/student\/programming\/perl\//)
{
    print "that pattern is in $file1 \n";
    $file1 =~ s/\/home\/student\/programming\/perl\///;
    print "we fixed it to be: $file1\n";
}
else
{
    print "no, that pattern is not in there";
}

#pattern goes in between two //'s
$file2 =~ s/\/home\/student\/programming\/perl\///;
print "we fixed file2 to be: $file2\n";

