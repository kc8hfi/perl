#!/usr/bin/perl

use strict;
use warnings;

print "type in some text: ";
my $input = <STDIN>;
$input =~ s/[\r\n]+//;


#substitution
# s/whatever you want to replace/ this is what to replace it with/

my $string = "thsi is some text that needs to be fixed";
print $string,"\n";
$string =~ s/thsi/this/;
print "fixed: " ,$string,"\n";

#pattern matching
#  /whatever you're looking for/

if ($string =~ /something else/)
{
    print "the word \"text\" is in there\n";
}
else
{
    print "it is NOT in there!\n";
}

