#!/usr/bin/perl

use strict;
use warnings;

my $name = "happy trails, buckaroo";

#split into an array
my @split = split (/a/,$name);


#split up and assign each piece to a variable
my $vehicle = "2013 Chevy Camaro";
my ($year,$make,$model) = split(/ /,$vehicle);
print $year,$make,$model,"\n";


#you can use split to parse a csv string,  but DONT DO THIS!
my $course = "84829,CS,365,T01,Computer Languages";
my ($crn,$subject,$course,$section,$title) = split(',',$course);
print $crn,$subject,$course,$section,$title,"\n";
