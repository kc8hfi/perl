#!/usr/bin/perl

use strict;
use warnings;

# more about hashes

#make a hash, with termcode and its description
my %termcodes = (
"201808"    =>  "Fall 2018",
"201901"    =>  "Spring 2019",
"201905"    =>  "Summer 2019"
);

#how many elements?
#keys returns the number of elements in the scalar context
my $howmany = keys %termcodes;
print "how many elements: $howmany\n";

my @keys = keys %termcodes;
foreach my $key(@keys)
{
    print "key: $key\n";
}

my @values = values (%termcodes);
foreach my $key(@values)
{
    print "value: $key\n";
}



