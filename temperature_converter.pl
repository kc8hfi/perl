#!/usr/bin/perl

use strict;
use warnings;

# this program will take user input and convert the temperature
# to the other unit of measure

print "Enter in a temperature followed by the unit of measurement\n";
print "for example,  -50F  if you want to turn -50F into celcius\n";
print "another example, 54C if you want to turn 54C into farenheit\n";

my $temp = <STDIN>;   #read something from the keyboard
$temp =~ s/[\r\n]+//; #get rid of new lines and carriage returns

# string should have a number with a letter
my $last_character = substr($temp,0,1);  #this gives us the very first character
$last_character = substr($temp,-1);  #this gives the last character

my $how_many_characters = length($temp);
my $number_part = substr($temp,0,$how_many_characters -1);

print "the number part is $number_part\n";
print "the last part is $last_character\n";

if ($last_character  eq "c" || $last_character eq "C")
{
	if($number_part =~ m/^[0-9]*$/)
	{
		my $converted = celcius($number_part);
		print "$temp is $converted in Farenheit\n";
	}
}
elsif ($last_character  eq "f" || $last_character eq "F")
{
	if($number_part =~ m/^[0-9]*$/)
	{
		my $converted = farenheit($number_part);
		print "$temp is $converted in Celcius\n";
	}
}



sub farenheit
{
	my $number = shift;
	#F to C
	#formula (farenheit -32) *5/9 = celcuis
	my $calculated = ($number - 32) * 5/ 9;
	return $calculated;	
}


sub celcius
{
	my $number = shift;
	#C to F formula
	#celcius * 9/5 + 32
	my $calculated = $number * 9 / 5 + 32;
	return $calculated;
}