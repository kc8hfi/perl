#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

#array of arrays
#push is the function to add an element to the end of an array

my @array1 = ("one","two");
print "array1\n";
print Dumper \@array1,"\n";

my @array2 = ("three","four");
print "array2\n";
print Dumper \@array2,"\n";

#this will actually take all the elements from @array2 and append them to the end of @array1
push (@array1,@array2);
print "after the first push\n";
print Dumper \@array1,"\n";


#We want an array of arrays
#so, we need to push the memory address of @array2 onto @array1
push(@array1, \@array2);
print "after pushing the reference\n";
print Dumper \@array1,"\n";
