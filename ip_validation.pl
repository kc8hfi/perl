#!/usr/bin/perl

use strict;
use warnings;

use Data::Validate::IP qw(is_ipv4 is_ipv6);

print "type in an ip address: ";
my $ip = <STDIN>;
$ip =~ s/[\r\n]+//;

if (is_ipv4($ip))
{
    print "$ip looks like an ipv4 adddress.\n";
}
else
{
    print "$ip does NOT look like an ipv4 address.\n";
}

my $ipv6 = "fe80::3c4c:a747:aa94:ec42";
if (is_ipv6($ipv6))
{
    print "this looks like an ipv6 address\n";
}
else{
    print "$ipv6 does NOT look like an ipv6 address.\n";
    }
