#!/usr/bin/perl

use strict;
use warnings;

use Email::Stuffer;
use Email::Sender::Transport::SMTP;

#command line arguments
#@ARGV

#print "how many arguments: " . scalar(@ARGV),"\n";

#foreach my $cmd(@ARGV)
#{
#     print "arg: " , $cmd , "\n";
#}

my $they_want_how_many;
if (scalar (@ARGV) == 1)
{
    $they_want_how_many = $ARGV[0];
}
elsif(scalar(@ARGV) == 0)
{
    $they_want_how_many = howmany();
}
else
{
    die "too many command line arguments!";
}

my @numbers;
for(my $i=0;$i<$they_want_how_many;$i++)
{
    #print int rand(10), "\n";
    my $lottery_number = int rand(10);
    print "Your $i number: $lottery_number\n";
    push(@numbers,$lottery_number);
}
sendemail(\@numbers);


sub howmany
{
    print "How many numbers for your lottery ticket? ";
    my $howmany = <STDIN>;
    $howmany =~ s/[\r\n]+//;
    return $howmany;
}

sub sendemail
{
    my $thenumbers = shift;
    my $to = "camey\@mix.wvu.edu";
    my $from = "rickjames\@mail.wvu.edu";
    my $subject = "Lottery Numbers";
    my $message = "";
    for my $item(@{$thenumbers})
    {
        $message = $message . "Number: $item\n";
    }

    my $email = new Email::Stuffer;
    $email->to($to);
    $email->from($from);
    $email->subject($subject);
    $email->text_body($message);
    $email->transport(Email::Sender::Transport::SMTP->new({
        host => "smtp.wvu.edu"}));

    $email->send();
}
