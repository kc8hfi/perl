#!/usr/bin/perl

use strict;
use warnings;

use Switch;

my @numbers=(1,2,5);

foreach my $x (@numbers)
{
    switch($x)
    {
        case 1
        {
            print "one\n";
        }
        case 5
        {
            print "x was 5\n";
        }
    }
}
