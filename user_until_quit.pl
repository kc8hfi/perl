#!/usr/bin/perl

use strict;
use warnings;

print "enter in a number (q = quit):";

my $number = <STDIN>;
$number =~ s/[\r\n]+//;

if ($number eq 'q')
{
    print "ok, we will quit now!\n";
}
else
{
    print "you entered a $number\n";
    print "now lets count to your $number\n";
    mycounter($number);
}


sub mycounter
{
    my $to = shift;
    for(my $i=0;$i<=$to;$i++)
    {
        print "$i\n";
    }
}
