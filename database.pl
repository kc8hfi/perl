#!/usr/bin/perl
use strict;
use warnings;

use DBI;
use Data::Dumper;

#connect to the database
my $host = "whatever the host is, could be the word localhost, or some ip address, or a hostname like www.google.com";
my $database = "the name of your database";
my $username = "whatever your database username is";
my $password = "whatever_the_password_is";


my $dsn ="DBI:mysql:database=$database;host=$host;port=3306";
my $mysqldb = DBI->connect($dsn,$username,$password);
if (!$mysqldb)
{
     die "error connecting to mysql; $DBI::errstr\n";
}

#your sql statement goes into a string variable
my $q = qq{
     select miles from 
     fuel_mileage

};
my $stmt = $mysqldb->prepare($q);
$stmt->execute();

#this will get the column headers from the query.  the NAME_uc means
#give the column names in all upper case.  replace the uc with lc
#to give the names in all lower letters
#OR just leave off the _uc and take whatever case the database gives you
my $colheaders = $stmt->{NAME_uc};
print Dumper $colheaders;

my $total_miles = 0;
while (my @row = $stmt->fetchrow_array())
{
     $total_miles = $total_miles + $row[0];
}
print "total miles: $total_miles\n";


$q = qq{
	select gallons from fuel_mileage
};
$stmt = $mysqldb->prepare($q);
$stmt->execute();
my $total_gallons = 0;
while (my @row = $stmt->fetchrow_array())
{
	$total_gallons += $row[0];
}
print "total gallons: $total_gallons\n";

my $mileage = $total_miles / $total_gallons;
print "fuel mileage: $mileage\n";
