#!/usr/bin/perl

use strict;
use warnings;

# more about hashes

#make a hash, with termcode and its description
my %termcodes = (
"201808"    =>  "Fall 2018",
"201901"    =>  "Spring 2019",
"201905"    =>  "Summer 2019"
);

#how to get all the values
my @values = values %termcodes;
foreach my $value(@values)
{
    print "value: $value\n";
}
