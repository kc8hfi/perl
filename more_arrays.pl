#!/usr/bin/perl
#
use strict;
use warnings;

#range operator
# ..
# to make a array with a range of numbers
my @numbers = (1 .. 10);

#array with letters
my @letters = ("a".."z");
my @caps = ("A".."Z");


#make another array
my @days = ("monday","tuesday","wednesday");

for my $element(@days)
{
     print $element,"\n";
}

print "add one\n";
#add element to the end
push(@days,"thursday");

for my $element(@days)
{
     print $element,"\n";
}

print "delete one\n";
#delete an element
#splice function, this removes the element at the specified index
#array to work with, the index, how many to remove;
splice (@days, 1,1);

for my $element(@days)
{
     print $element,"\n";
}


#pop
#this pops an element off the array
my $last_one = pop(@days);
print "last element: $last_one\n";
print "how many left: " . scalar(@days),"\n";
for my $element(@days)
{
     print $element,"\n";
}

#put sunday on the front
unshift (@days,"sunday");
print "sunday should be on the front\n";
for my $element(@days)
{
     print $element,"\n";
}

my $firstone = shift(@days);







