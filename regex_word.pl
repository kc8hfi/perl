#!/usr/bin/perl

use strict;
use warnings;


my $string = "Welcome to perl!\n";

if ($string =~ m/\b/)
{
    print "this is a good sentence\n";
}

$string = "runonsentencethaticannotread";
if ($string =~ m/\b/)
{
    print "it is a good sentence also\n";
}
else
{
    print "hey, does your spacebar not work!?\n";
}
    
