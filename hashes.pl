#/usr/bin/perl

use strict;
use warnings;

# this is all about hashes
# hashes are key=>value pairs
# 
# key=>value

#declare and initialize a hash
my %colors = (
'red' => 'apple',
'yellow'=>'banana'
);
$colors{'purple'} = "grape";

while(my($key,$value) = each(%colors))
{
    print "$key=>$value\n";
}



