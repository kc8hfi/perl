#!/usr/bin/perl

use strict;
use warnings;

#build a class list from user input

print "Your class schedule\n";
print "You will be prompted to enter in the subject, course, and title for each of your classes\n";
print "Typing the word \"quit\" any time will quit\n";

print "Do you want to start adding classes? [yes/no]";
my $input = <STDIN>;
$input =~ s/[\r\n]+//;

#need array to hold all my classes
my @all_courses = ();


while ($input ne "quit")
{
     #need array to hold all the info for a single class
     my @single_course = ();

     print "Subject: ";
     $input = <STDIN>;
     $input =~ s/[\r\n]+//;
     if ($input eq "quit")
     {
          last;
     }
     else
     {
          push(@single_course,$input);
     }
     
     print "Course: ";
     $input = <STDIN>;
     $input =~ s/[\r\n]+//;
     if ($input eq "quit")
     {
          last;
     }
     else
     {
          push(@single_course,$input);
     }
     
     print "Title: ";
     $input = <STDIN>;
     $input =~ s/[\r\n]+//;
     if ($input eq "quit")
     {
          last;
     }
     else
     {
          push(@single_course,$input);
     }
     push(@all_courses,\@single_course);
     
}
print "\n\n\n";
print @all_courses,"\n";

