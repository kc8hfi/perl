#!/usr/bin/perl

use strict;
use warnings;

# more about hashes

#make a hash, with termcode and its description
my %termcodes = (
"201808"    =>  "Fall 2018",
"201901"    =>  "Spring 2019",
"201905"    =>  "Summer 2019"
);

#another way to loop using while
while(my ($key,$value) = each(%termcodes))
{
     print "$key=>$value\n";
}
print "\n\n\n";

#add an element
$termcodes{"201708"} = "Fall 2018";
#another way to loop using while
while(my ($key,$value) = each(%termcodes))
{
     print "$key=>$value\n";
}
print "\n\n\n";

#add another item to the hash
$termcodes{"201705"} = "Summer 2017";
$termcodes{"yaar"} = "git r done";


#delete an element
delete $termcodes{"201708"};
#another way to loop using while
while(my ($key,$value) = each(%termcodes))
{
     print "$key=>$value\n";
}

delete($termcodes{"yaar"});

