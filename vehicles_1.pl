#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Text::CSV_XS;

my $csv = Text::CSV_XS->new( 
    {
        always_quote => 1, binary=>1
    }
);

open my $fh, "<vehicles.csv" or die "cannot open file, $!";

my @colheaders = @{$csv->getline($fh)};
my $row = {};
$csv->bind_columns(\@{$row}{@colheaders});

while($csv->getline($fh))
{
    print Dumper $row;
}

close $fh;
