#!/usr/bin/perl
#
use strict;
use warnings;

open my $handle, "</etc/passwd" or die "cannot open the file!  $! \n";
while (my $line = <$handle>)
{
	my @stuff = split(':',$line);
	print "username: " . $stuff[0],"\n";
}
close $handle;
