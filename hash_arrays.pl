#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my %cpus;
$cpus{"ryzen 3"} = ["1200","1300X","200G"];
$cpus{"ryzen 5"} = ["1400","1500X","1600","2600","1600X","2600X","2400G"];
$cpus{"ryzen 7"} = ["1700","2700","1700X","2700X","1800X"];
$cpus{"threadripper"} = ["1900X","1920X","1950X"];

#get all the keys in this big hash
my @keys = keys(%cpus);
print @keys;


print Dumper(\%cpus);
