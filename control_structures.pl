#!/usr/bin/perl

use strict;
use warnings;

#control structures

my $vehicle = "truck";

my $vehicle2 = "car";


#this is for strings
#eq = means equal to
#ne = means not equal to

#this is for numbers
# == means equal to
# != means not equal to
# <, >, <=, >=

if ($vehicle == $vehicle2)
{
    print "these are the same\n";
}
else
{
    print "not the same\n";
}

my $year1 = 2013;
my $year2 = 1999;
if ($year1 < $year2)
{
    print "$year1 is less than $year2 \n";
}
else
{
    print "$year1 is greater than $year2 \n";
}


my $a1 = 10;
my $a2 = 11;
my $a3 = 12;
my $a4 = 13;
if ($a1>$a2)
{
    print "$a1 greater than $a2";
}
elsif($a1<$a3)
{
    print "$a1 less than $a3";
}
else
{
    print "$a1 is $a1";
}


