#!/usr/bin/perl

use strict;
use warnings;

use Data::Dumper;

my @letters = ("a".."z");

print Dumper(\@letters);

my %termcodes = (
"201808"    =>  "Fall 2018",
"201901"    =>  "Spring 2019",
"201905"    =>  "Summer 2019"
);

print Dumper(\%termcodes);



