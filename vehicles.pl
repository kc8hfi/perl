#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Text::CSV_XS;


my $csv = Text::CSV_XS->new( 
    {
        always_quote => 1, binary=>1
    }
);

open my $fh, "<vehicles.csv" or die "cannot open file, $!";

my $headers = <$fh>;
# while(my $line = <$fh>)
# {
#     print $line;
#     $csv->parse($line);
#     my @fields = $csv->fields();
# }

my @years;
my @makes;
my @models;
my @engines;
my @codes;
my @transmissions;

#my @colheaders = @{$csv->getline($fh)};
#my $row = {};
#$csv->bind_columns(\@{$row}{@colheaders});

while(my $line = $csv->getline($fh))
{
    #get an element ffrom an array
    # $array[0], $array[1], and so on
    #print $line->[0],"\n";
    #print $line->[1],"\n";
    push(@years, $line->[0]);
    push(@makes, $line->[1]);
    push(@models, $line->[2]);
    push(@engines, $line->[3]);
    push(@codes, $line->[4]);
    push(@transmissions, $line->[5]);
}

print Dumper(\@years);
print Dumper(\@makes);
print Dumper(\@models);
print Dumper(\@engines);
print Dumper(\@codes);
print Dumper(\@transmissions);

close $fh;
