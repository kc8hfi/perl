#!/usr/bin/perl

use strict;
use warnings;

use lib '/home/camey/programming/perl/packages';
use mileage;

my $using_mileage = mileage->new (100,10,30);
#print $using_mileage->toString();

print "miles: " . $using_mileage->getMiles(),"\n";


print "update miles: ";
$using_mileage->setMiles();
print "miles: " . $using_mileage->getMiles(),"\n";



my $another = mileage->new();
print $another->toString(),"\n";


