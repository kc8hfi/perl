package mileage;

use strict;

sub new
{
     my $class_name = shift;
     my $self = {
          _miles => shift,
          _gallons => shift,
          _cost => shift
     };
     bless $self,$class_name;
     return $self;
}

sub toString
{
     my $self = shift;
     my $string = "Miles: $self->{_miles} Gallons: $self->{_gallons} Cost: $self->{_cost}";
     return $string;
}

sub getMiles
{
     my $self = shift;
     return $self->{_miles};
}

sub setMiles
{
     my $self = shift;
     my $m = shift;
     if (defined($m))
     {
          $self->{_miles} = $m;
     }
}

sub getGallons
{
     my $self = shift;
     return $self->{_gallons};
}

sub setGallons
{
     my $self = shift;
     my $m = shift;
     if (defined($m))
     {
          $self->{_gallons} = $m;
     }
}

sub getCost
{
     my $self = shift;
     return $self->{_cost};
}

sub setCost
{
     my $self = shift;
     my $m = shift;
     if (defined($m))
     {
          $self->{_cost} = $m;
     }
}




1;
