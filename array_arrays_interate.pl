#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

#make an array with arrays inside it

my @bigarray = ();

my @littlearray1 = ("2013","camaro");
my @littlearray2 = ("1999","s10");


#push the first array onto the big one
push(@bigarray,\@littlearray1);

#push the second array onto the big one
push(@bigarray,\@littlearray2);

print Dumper \@bigarray,"\n";

#iterate through the bigarray
foreach my $smallarray(@bigarray)
{
#     print "item:\n";
#     print Dumper $smallarray,"\n";
    foreach my $item(@{$smallarray})
    {
        print "little item: $item\n";
    }
    
}
