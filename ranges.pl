#!/usr/bin/perl

use strict;
use warnings;

#array with the numbers from 1 to 10
my @myarray = (1..10);

#array with the letters a,b,c,d,e,f,g
my @letters = ("a".."g");

#capital letters
my @capital_letters = ("A".."G");


foreach my $number(@myarray)
{
    print "$number,";
}
print "\n";

foreach my $letter(@letters)
{
    print $letter,",";
}
print "\n";

foreach my $letter(@capital_letters)
{
    print "$letter,";
}
print "\n";
