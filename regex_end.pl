#!/usr/bin/perl

use strict;
use warnings;

my $string = "are we at the end?";
if ($string =~ m/end$/)
{
    print "it is at the end!\n";
}
else
{
     print "end is not at the end of '$string'\n";
}

if ($string =~ m/end\?$/)
{
    print "it is at the end\n";
}
else
{
    print "no, not there at the end\n";
}

$string = "stop this nonsense";
if ($string =~ m/sense$/)
{
     print "the chars 'sense' is at the end of  '$string'\n";
}

$string = "gitrdone";
if ($string =~ m/ne$/)
{
     print "ne is at the end of '$string'\n";
}

$string = "big ugly      carter";
if ($string =~ m/ carter$/)
{
     print "a space ' carter' is at the end of '$string'\n";
}


$string = "big uglycarter";
if ($string =~ m/ carter$/)
{
     print "a space ' carter' is at the end of '$string'\n";
}
else
{
     print "there was no space and then carter at the end of '$string'\n";
}


my $number = 42;
if ($number =~ m/2$/)
{
     print "2 is the last number in $number\n";
}

