#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Scalar::Util qw(looks_like_number);

#here is what an IP address looks like
#   69.161.224.85


print "give me your ip address: ";
my $ip = <STDIN>;
$ip =~ s/[\r\n]+//;


#my $temp = $ip;
#$temp =~ s/\.//g;

print $ip,"\n";
#my @pieces = split("\.",$ip);
my @pieces = split(/\./,$ip);

if (scalar(@pieces) != 4)
{
    die "this is not an ip address! you fail\n";
}

print Dumper(\@pieces);
my $is_ok = 1;
for my $piece(@pieces)
{
    if (looks_like_number($piece))
    {
        print "$piece is ok\n";
    }
    else
    {
        $is_ok = 0;
        last;
    }
}
if ($is_ok == 1)
{
    print "yes, $ip is a good number\n";
}
else
{
    print "no, $ip is not a good number, you fail!\n";
}
    
