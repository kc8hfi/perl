#!/usr/bin/perl

use strict;
use warnings;


#defined or not
#defined returns a boolean value
#if the expression contains the value "undef", it returns false.
#otherwise, it returns true

my $number = 10;

if (!defined $number)
{
     print "number is not defined\n";
}
else
{
    print "number is defined\n";
}

my $x;
if (defined($x))
{    
     print "x is defined\n";
}
else
{
     print "x is NOT defined!\n";
}
