#!/usr/bin/perl

use strict;
use warnings;

# more about hashes

#make a hash, with termcode and its description
my %termcodes = (
"201808"    =>  "Fall 2018",
"201901"    =>  "Spring 2019",
"201905"    =>  "Summer 2019"
);

#loop through a hash
foreach my $key(keys %termcodes)
{
     print "key:$key value:$termcodes{$key}\n";
}

#another way to loop using while
while(my ($key,$value) = each(%termcodes))
{
     print "$key=>$value\n";
}

