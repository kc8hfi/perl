#!/usr/bin/perl

use strict;
use warnings;

my $string = "the word front is in here somewhere but not at the front";
if ($string =~ m/^front/)
{
    print "it is at the beginning\n";
}
else
{
     print "^front is not at the beginning of '$string'\n";
}

$string = "front is at the beginning";
if ($string =~ m/^front/)
{
     print "^front is at the front of '$string'\n";
}


$string = "208 carter";
if ($string =~ m/^208/)
{
     print "'$string' is 208\n";
}

$string = "205 carter";
if ($string =~ m/^208/)
{
     print "'$string' is 208\n";
}
else
{
     print "'$string' is NOT 208!!!\n";
}

my $number = 1999;
if ($number =~ m/^19/)
{
     print "this is the 1900's\n";
}
