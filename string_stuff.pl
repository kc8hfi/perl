#!/usr/bin/perl

use strict;
use warnings;

my $bigstring = "Happy Halloween!";

print "how big: " . length($bigstring)."\n";

my $text = "howdy people!";

#substr( the string to work with,  the starting position, how many positions to read)
my $first = substr($text,0,1);

print "first character: $first\n";

#loop through every character in the string
for my $i (0 .. length($text)-1 )
{
    my $character = substr($text,$i,1);
    print "index: $i character: $character";
    print "\n";
}


#split a string up and put each characer into an array element

