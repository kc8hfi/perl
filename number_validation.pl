#!/usr/bin/perl

use strict;
use warnings;

#number validation
# if we ask the user for numbers, make sure they type in numbers

print "enter in a single number:  ";
my $number = <STDIN>;
$number =~ s/[\r\n]+//;

if (input_ok($number)== 1)
{
     print "Good job!";
}
else
{
     print "You fail! ";
}
print "\n";



sub input_ok
{
     my $x = shift;
     my $is_digit = 0;
     my @digits = (0..9);
     foreach my $digit(@digits)
     {
          if ($digit == $x)
          {
               $is_digit = 1;
               last;
          }
     }
     return $is_digit;
}
